# -*- coding: utf-8 -*-

import numpy as np

# rows = patients (60), columns = days (40)
FILENAME = '../data/tutorial_inflammation/inflammation-01.csv'

data = np.loadtxt(FILENAME, delimiter=',')

# How can we select only those patients with a maximum value of 19 and larger?
patient_ix = data.max(axis=1) >= 19.
print('patient_ix is an array with ' + str(patient_ix.size) + ' entries')
print(patient_ix)
print('There are ' + str(data[patient_ix].shape[0]) + ' patients with 19 or more')